# CentraleSupélec Coding WEEKS : Data analysis and visualization with python.

## Welcome to Coding Weeks!
### This space corresponds to the **datavisualization** project of the first week of the coding weeks.

This two-week activity will allow you to improve your programming and development skills. 

At the end of this period, you will be able to create a game of your choice in Python.

This course will be divided in two sequences. The first week will be dedicated to learning useful concepts, through practice, by programming a `data visualization application`. The second week will be a project week where you will have to choose one or more public datasets and enhance them through their analysis and visualization.


## Prerequisite

### Join the activity spaces

Before you begin, please:

- join the TEAMS team code **x2f0146** 

- divide yourself into teams of five and note your team on this [binder](https://centralesupelec.sharepoint.com/:x:/s/CS_Coding_Week_DataViz_2021_2022/EUxquxnIBLpDnppjlUm9ZicB8x-Dyuj9UXMfbtcgpXOZEg?e=Th1Lsv),
- join the coding weeks slack from this [invitation](https://join.slack.com/t/codingweeks20212022/shared_invite/zt-xvxh6pia-FCIuGk_0jR9Whv9NBqJfAg)
- and join at least the `#cw_week1_dataviz` and `#cw_important_information` channels
 on Slack.

> Slack is a collaborative communication platform widely used in companies. It's how you can chat with each other, share code and receive additional information from managers. You can either download the Slack application or use the online version.
	

### Visual Studio Code (VSCode)

For these two weeks, we strongly recommend that you install and use [Visual Studio Code](https://code.visualstudio.com/) (not to be confused with Visual Studio) which is an editor that has become very popular in recent years. It is very suitable for python and, above all, it is very easy to learn and understand. 

You can use another editor if you wish. It will then be your responsibility to know how to use it.

* :point_right: _[I don't know how to use VSCode](https://github.com/hudelotc/CentraleSupelec_CodingWeeks_2020/blob/main/VisualStudioCode.md)._
* :point_right: _[I don't know how to use VSCode : keyboard shortcuts](https://github.com/LoicPoullain/je-code/blob/master/utiliser-visual-studio-code.md)._

### A quick reminder of Bash

During these two weeks, you will have to use the shell very often. We invite you to follow the short tutorial below to gain confidence.


:point_right: _[Cheatsheet Bash](https://github.com/hudelotc/CentraleSupelec_CodingWeeks_2020/blob/main/bash.md)._



### Git

Git is one of the most important tools in coding weeks. It is the one that will allow you to work with several people on the same project.

To allow you to master this tool, we propose you :

* A course on Git which is available [here](https://web.microsoftstream.com/video/ec2b9aa4-f1c4-42dc-994d-f99b767992d1) or on Edunao
* A small tutorial to do on Git with the help of [Learn Git Branching](https://learngitbranching.js.org/) which is available [here](https://github.com/hudelotc/CentraleSupelec_CodingWeeks_2020/blob/main/Git.md)


### Python

Following the SIP course, you should all have python installed on your machine. If you don't, please refer to the instructions given during the course.

To start this week, we suggest you to follow the different small tutorials below about python:

* :point_right: _[Know and check your python version ](https://github.com/hudelotc/CentraleSupelec_CodingWeeks_2020/blob/main/pythonversion.md)._
* :point_right: _[Importing files and modules
 ](https://github.com/hudelotc/CentraleSupelec_CodingWeeks_2020/blob/main/modulespackagespython.md)._
* :point_right: _[Exceptions](https://github.com/hudelotc/CentraleSupelec_CodingWeeks_2020/blob/main/exceptions.md)._
 
 
 Before going on, you have to pass the 3 tests on Edunao if you haven't already done so:
 
* [Test bash](https://centralesupelec.edunao.com/mod/quiz/view.php?id=73157)
* [Test Git](https://centralesupelec.edunao.com/mod/quiz/view.php?id=72936)
* [Test Jalon 0](https://centralesupelec.edunao.com/mod/quiz/view.php?id=72937)
 







## Week 1 project



You are now ready. Let's get started!

### To start, it's [here](./TemplateProject_dataviz.md)



